﻿//----------------------------------------------------------------
// shopping cart
//
function shoppingCart(cartName) {
    this.cartName = cartName;
    this.clearCart = false;
    this.checkoutParameters = {};
    this.items = [];
    this.order_info = {};
    this.your_details = [];
    this.your_delivery_details = [];
    // load items from local storage when initializing
    this.loadItems();
    this.loadOrderInfo();
    this.loadYourDetails();
    this.loadYourDeliveryDetails();

    // save items to local storage when unloading
    var self = this;
   // $(window).unload(function () {
        if (self.clearCart) {
            self.clearItems();
        }
        self.saveItems();
        self.clearCart = false;
   // });
}

// load items from local storage
shoppingCart.prototype.loadItems = function () {
    var items = localStorage != null ? localStorage[this.cartName + "_items"] : null;
    if (items != null && JSON != null) {
        try {
            var items = JSON.parse(items);
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.sku != null && item.name != null && item.price != null && item.quantity != null) {
                    item = new cartItem(item.sku, item.name, item.price, item.quantity);
                    this.items.push(item);
                }
            }
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}


// load Order Info from local storage
shoppingCart.prototype.loadOrderInfo = function () {
    var orderInfo = localStorage != null ? localStorage[this.cartName + "_order_info"] : null;
    if (orderInfo != null && JSON != null) {
        try {
            var orderInfo = JSON.parse(orderInfo);
          
            for (var i = 0; i < orderInfo.length; i++) {
                var orderi = orderInfo[i];
               if (orderi.order_for != null && orderi.payment_method != null && orderi.time != null && orderi.delivery_price != null) {
                    orderinfo = new orderInfoItem(orderi.order_for, orderi.payment_method, orderi.time, orderi.delivery_price);
                    this.order_info.push(orderinfo);
                }
            }
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}

// load Your details from local storage
shoppingCart.prototype.loadYourDetails = function () {
    var yourDetails = localStorage != null ? localStorage[this.cartName + "_your_details"] : null;
    
    if (yourDetails != null && JSON != null) {
        try {
            var yourDetails = JSON.parse(yourDetails);

               if (yourDetails.title != null && yourDetails.fname != null && yourDetails.lname != null && yourDetails.cno != null && yourDetails.email != null) {
                    
                    your_details = new orderYourDetailItem(yourDetails.title, yourDetails.fname, yourDetails.lname, yourDetails.cno , yourDetails.email);
                    this.your_details = your_details;
               }
    
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}

// load Your details from local storage
shoppingCart.prototype.loadYourDeliveryDetails = function () {
    var yourDeliveryDetails = localStorage != null ? localStorage[this.cartName + "_your_delivery_details"] : null; 
    if (yourDeliveryDetails != null && JSON != null) {
        try {
            var yourDeliveryDetails = JSON.parse(yourDeliveryDetails);

               if (yourDeliveryDetails.house_no != null && yourDeliveryDetails.street != null && yourDeliveryDetails.town != null && yourDeliveryDetails.postcode != null) {
                    your_delivery_detail = new orderYourDeliveryDetailItem(yourDeliveryDetails.house_no, yourDeliveryDetails.street, yourDeliveryDetails.town, yourDeliveryDetails.postcode);
                    this.your_delivery_details = your_delivery_detail;
               }
    
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}

// save items to local storage
shoppingCart.prototype.saveItems = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.cartName + "_items"] = JSON.stringify(this.items);
    }
}

// save order info to local storage
/*shoppingCart.prototype.saveOrderInfo = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.cartName + "_order_info"] = JSON.stringify(this.order_info);
    }
}*/

// save your details  to local storage
shoppingCart.prototype.saveYourDetails = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.cartName + "_your_details"] = JSON.stringify(this.your_details);
    }
}


// save your delivery details to local storage
shoppingCart.prototype.saveYourDeliveryDetails = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.cartName + "_your_delivery_details"] = JSON.stringify(this.your_delivery_details);
    }
}

// adds an item to the cart
shoppingCart.prototype.addItem = function (sku, name, price, type, quantity) {
   
    quantity = this.toNumber(quantity);
    if (quantity != 0) {

        // update quantity for existing item
        var found = false;
        for (var i = 0; i < this.items.length && !found; i++) {
            var item = this.items[i];
           
            if (item.sku == sku && item.type == type) {
                found = true;
                item.quantity = this.toNumber(item.quantity + quantity);
                if (item.quantity <= 0) {
                    this.items.splice(i, 1);
                }
            }
            
        }

        // new item, add now
        if (!found) {
            var item = new cartItem(sku, name, price, type, quantity);
            this.items.push(item);
        }

        // save changes
        this.saveItems();
    }
}



// adds Your Details to cart
shoppingCart.prototype.addYourDetails= function () {
   
    var title = $("#title").val();
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var cno = $("#cno").val();

    //Validations for the fields
    if ( title == ""){
     //   alert('Please fill title');
    }
    
    
    var details = new orderYourDetailItem(title, fname, lname, cno, email);
    this.your_details = details;
    this.saveYourDetails();
    
}


// adds Your Delivery Details to cart
shoppingCart.prototype.addYourDeliveryDetails= function () {
   
    var house_no = $("#house_no").val();
    var street = $("#street").val();
    var town = $("#town").val();
    var postcode = $("#postcode").val();
    
    //Validations for the fields
    if ( house_no == ""){
     //   alert('Please fill house_no');
    }
    
   
    var details = new orderYourDeliveryDetailItem(house_no, street, town, postcode);
    this.your_delivery_details = details;
    this.saveYourDeliveryDetails();
    
}



// get the total price for all items currently in the cart
shoppingCart.prototype.getTotalPrice = function (sku) {
    var total = 0;
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (sku == null || item.sku == sku) {
            total += this.toNumber(item.quantity * item.price);
        }
    }
    return total;
}

// get the total price for all items currently in the cart
shoppingCart.prototype.getTotalCount = function (sku) {
    var count = 0;
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (sku == null || item.sku == sku) {
            count += this.toNumber(item.quantity);
        }
    }
    return count;
}

// get the main total price for all items currently in the cart
shoppingCart.prototype.getMainTotalPrice = function (sku) {
   var total = 0;
   var delivery_price = 0;
   var main_total = 0;
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (sku == null || item.sku == sku) {
            total += this.toNumber(item.quantity * item.price);
        }
    }
    var item_price_total = total;
    var order_for = this.order_info.order_for;
    if(order_for == "1") {
        delivery_price = this.order_info.delivery_price;
    }
    main_total = item_price_total +delivery_price;
    return main_total;
}

// get the delivery price for all items currently in the cart
shoppingCart.prototype.getDeliveryPrice = function (sku) {
    var dprice = 1.50;
    return dprice;
}

// get the delivery price for all items currently in the cart
shoppingCart.prototype.getDiscountPrice = function (sku) {
    var dprice = 0.00;
   
    return dprice;
}

// clear the cart
shoppingCart.prototype.clearItems = function () {
    this.items = [];
    this.saveItems();
    
    this.order_info = {};
    //this.saveOrderInfo();
    
    this.your_details = [];
    this.saveYourDetails();
}

// define checkout parameters
shoppingCart.prototype.addCheckoutParameters = function (serviceName, merchantID, options) {

    // check parameters
    if (serviceName != "PayPal" && serviceName != "Google" && serviceName != "Stripe") {
        throw "serviceName must be 'PayPal' or 'Google' or 'Stripe'.";
    }
    if (merchantID == null) {
        throw "A merchantID is required in order to checkout.";
    }

    // save parameters
    this.checkoutParameters[serviceName] = new checkoutParameters(serviceName, merchantID, options);
}

// check out
shoppingCart.prototype.checkout = function (serviceName, clearCart) {

    // select serviceName if we have to
    if (serviceName == null) {
        var p = this.checkoutParameters[Object.keys(this.checkoutParameters)[0]];
        serviceName = p.serviceName;
    }

    // sanity
    if (serviceName == null) {
        throw "Use the 'addCheckoutParameters' method to define at least one checkout service.";
    }

    // go to work
    var parms = this.checkoutParameters[serviceName];
    if (parms == null) {
        throw "Cannot get checkout parameters for '" + serviceName + "'.";
    }
    switch (parms.serviceName) {
        case "PayPal":
            this.checkoutPayPal(parms, clearCart);
            break;
        case "Google":
            this.checkoutGoogle(parms, clearCart);
            break;
        case "Stripe":
            this.checkoutStripe(parms, clearCart);
            break;
        default:
            throw "Unknown checkout service: " + parms.serviceName;
    }
}

// check out using PayPal
// for details see:
// www.paypal.com/cgi-bin/webscr?cmd=p/pdn/howto_checkout-outside
shoppingCart.prototype.checkoutPayPal = function (parms, clearCart) {

    // global data
    var data = {
        cmd: "_cart",
        business: parms.merchantID,
        upload: "1",
        rm: "2",
        charset: "utf-8"
    };

    // item data
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        var ctr = i + 1;
        data["item_number_" + ctr] = item.sku;
        data["item_name_" + ctr] = item.name;
        data["quantity_" + ctr] = item.quantity;
        data["amount_" + ctr] = item.price.toFixed(2);
    }

    // build form
    var form = $('<form/></form>');
    form.attr("action", "https://www.paypal.com/cgi-bin/webscr");
    form.attr("method", "POST");
    form.attr("style", "display:none;");
    this.addFormFields(form, data);
    this.addFormFields(form, parms.options);
    $("body").append(form);

    // submit form
    this.clearCart = clearCart == null || clearCart;
    form.submit();
    form.remove();
}

// check out using Google Wallet
// for details see:
// developers.google.com/checkout/developer/Google_Checkout_Custom_Cart_How_To_HTML
// developers.google.com/checkout/developer/interactive_demo
shoppingCart.prototype.checkoutGoogle = function (parms, clearCart) {

    // global data
    var data = {};

    // item data
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        var ctr = i + 1;
        data["item_name_" + ctr] = item.sku;
        data["item_description_" + ctr] = item.name;
        data["item_price_" + ctr] = item.price.toFixed(2);
        data["item_quantity_" + ctr] = item.quantity;
        data["item_merchant_id_" + ctr] = parms.merchantID;
    }

    // build form
    var form = $('<form/></form>');
    // NOTE: in production projects, use the checkout.google url below;
    // for debugging/testing, use the sandbox.google url instead.
    //form.attr("action", "https://checkout.google.com/api/checkout/v2/merchantCheckoutForm/Merchant/" + parms.merchantID);
    form.attr("action", "https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/" + parms.merchantID);
    form.attr("method", "POST");
    form.attr("style", "display:none;");
    this.addFormFields(form, data);
    this.addFormFields(form, parms.options);
    $("body").append(form);

    // submit form
    this.clearCart = clearCart == null || clearCart;
    form.submit();
    form.remove();
}

// check out using Stripe
// for details see:
// https://stripe.com/docs/checkout
shoppingCart.prototype.checkoutStripe = function (parms, clearCart) {

    // global data
    var data = {};

    // item data
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        var ctr = i + 1;
        data["item_name_" + ctr] = item.sku;
        data["item_description_" + ctr] = item.name;
        data["item_price_" + ctr] = item.price.toFixed(2);
        data["item_quantity_" + ctr] = item.quantity;
    }

    // build form
    var form = $('.form-stripe');
    form.empty();
    // NOTE: in production projects, you have to handle the post with a few simple calls to the Stripe API.
    // See https://stripe.com/docs/checkout
    // You'll get a POST to the address below w/ a stripeToken.
    // First, you have to initialize the Stripe API w/ your public/private keys.
    // You then call Customer.create() w/ the stripeToken and your email address.
    // Then you call Charge.create() w/ the customer ID from the previous call and your charge amount.
    form.attr("action", parms.options['chargeurl']);
    form.attr("method", "POST");
    form.attr("style", "display:none;");
    this.addFormFields(form, data);
    this.addFormFields(form, parms.options);
    $("body").append(form);

    // ajaxify form
    form.ajaxForm({
        success: function () {
            $.unblockUI();
            alert('Thanks for your order!');
        },
        error: function (result) {
            $.unblockUI();
            alert('Error submitting order: ' + result.statusText);
        }
    });

    var token = function (res) {
        var $input = $('<input type=hidden name=stripeToken />').val(res.id);

        // show processing message and block UI until form is submitted and returns
        $.blockUI({ message: 'Processing order...' });

        // submit form
        form.append($input).submit();
        this.clearCart = clearCart == null || clearCart;
        form.submit();
    };

    StripeCheckout.open({
        key: parms.merchantID,
        address: false,
        amount: this.getTotalPrice() *100, /** expects an integer **/
        currency: 'usd',
        name: 'Purchase',
        description: 'Description',
        panelLabel: 'Checkout',
        token: token
    });
}

// utility methods
shoppingCart.prototype.addFormFields = function (form, data) {
    if (data != null) {
        $.each(data, function (name, value) {
            if (value != null) {
                var input = $("<input></input>").attr("type", "hidden").attr("name", name).val(value);
                form.append(input);
            }
        });
    }
}
shoppingCart.prototype.toNumber = function (value) {
    value = value * 1;
    return isNaN(value) ? 0 : value;
}

//----------------------------------------------------------------
// checkout parameters (one per supported payment service)
//
function checkoutParameters(serviceName, merchantID, options) {
    this.serviceName = serviceName;
    this.merchantID = merchantID;
    this.options = options;
}

//----------------------------------------------------------------
// items in the cart
//
function cartItem(sku, name, price, type, quantity) {
    this.sku = sku;
    this.name = name;
    this.price = price * 1;
    this.type = type;
    this.quantity = quantity * 1;
}

//----------------------------------------------------------------
// items in the order info
//
function orderInfoItem(order_for,payment_method,time, delivery_price, subtotal, total) {
    this.order_for = order_for;
    this.payment_method = payment_method;
    this.time = time;
    this.delivery_price = delivery_price;
    this.subtotal = subtotal;
    this.total = total;
}


//----------------------------------------------------------------
// items in the order info
//
function orderInfoItemOrderFor(order_for) {
    this.order_for = order_for;

}

//----------------------------------------------------------------
// items in the order info
//
function orderInfoItemPaymentMethod(payment_method) {
    this.payment_method = payment_method;

}


//----------------------------------------------------------------
// Order Your Details Item 
//
function orderYourDetailItem(title, fname, lname, cno, email ) {
    this.title = title;
    this.fname = fname;
    this.lname = lname;
    this.cno = cno;
    this.email = email;
}

//----------------------------------------------------------------
// Order Your Delivery Details Item 
//
function orderYourDeliveryDetailItem(house_no, street, town, postcode) {
    this.house_no = house_no;
    this.street = street;
    this.town = town;
    this.postcode = postcode;
   
}



// adds Order information to cart
shoppingCart.prototype.addOrderInfo= function ($scope) {
    
   
    var time = $("#time option:selected").val();
    var subtotal = this.getTotalPrice();
    var total = this.getMainTotalPrice();
    
    this.order_info['time'] = time;
    this.order_info['subtotal'] = subtotal;
    this.order_info['total'] = total;
   
    //var order = new orderInfoItem(order_for,payment_method,time, delivery_price, subtotal, total);
   
}


// check out order action on Type of Delivery Collection/Delivery buttons
shoppingCart.prototype.orderDeliveryAction = function ($scope,$type) {
     
    var orderfor = $scope.order_for;
    var option = orderfor;
    
    //this.order_info.pop();
    var order = new orderInfoItemOrderFor(orderfor);
    this.order_info['order_for'] = orderfor;
    if ($type == 1) {
         this.order_info['delivery_price'] = this.getDeliveryPrice();
    }else{
          this.order_info['delivery_price'] = 0;
    }
    
  //  this.order_info.push(order);
    //this.saveOrderInfo();
    
    switch (option) {
        case "2":
            $( "#delivery_row" ).hide();
            break;
        case "1":
            $( "#delivery_row" ).show();
            break;
        default:
            throw "Unknown  service: ";
    }
}



// check out order action on buttons
shoppingCart.prototype.orderPaymentMethodAction = function ($scope) {
    
    var payment_method = $scope.payment_method;
    //this.order_info.pop();
    var order = new orderInfoItemPaymentMethod(payment_method);
    this.order_info['payment_method'] = payment_method;
    //this.order_info.push(order);
    //this.saveOrderInfo();
}
