angular.module('starter.controllers', [])




.controller('LoginCtrl', function($scope, ShoppingCart) {
  
   $scope.redirect = function($loc) { 
     $location.path('/#/tab/dash');
  };
   $scope.cart = ShoppingCart.cart; 
 })

.controller('DashCtrl', function($scope) {})

/*
 * $Controller: Manage All the Menu Categories
 */
.controller('MenuCategoriesCtrl', function($scope, $http,  Ditems) {

   Ditems.getCast().then(function (asyncCastData) {
           $scope.ditems = asyncCastData;          
    });

})


/*
 * $Controller:Manage Menu Items and the cart conent
 */
.controller('MenuItemsCtrl', function($scope, $stateParams, $http, Ditems, MenuItems, ShoppingCart) {
 
   var menuItems = MenuItems.getMenuItemsByCategoryId($stateParams.cId);
   menuItems.getCast().then(function (asyncCastData) {
           $scope.menuItems = asyncCastData;
           
    });
 
  // get store and cart from service
  $scope.cart = ShoppingCart.cart;
 
 
})


/*
 * $Controller:Checkout
 */
.controller('CheckoutCtrl', function($scope, ShoppingCart) {
  
   $scope.pageTitle = "Checkout";
   $scope.cart = ShoppingCart.cart;
 
 
  
})

/*
 * $Controller:Order Details
 */
.controller('OrderDetailsCtrl', function($scope, ShoppingCart, $ionicNavBarDelegate) {
  
   $scope.pageTitle = "Order Details";
   $scope.cart = ShoppingCart.cart;
   $ionicNavBarDelegate.showBackButton(true);
 
  
})

/*
 * $Controller:Yours Details
 */
.controller('YourDetailsCtrl', function($scope, ShoppingCart, $ionicNavBarDelegate) {
 
   $scope.pageTitle = "Your Details";
   $scope.cart = ShoppingCart.cart;
   $ionicNavBarDelegate.showBackButton(true);
  
})

/*
 * $Controller:Yours Deliveries Details
 */
.controller('YourDeliveryDetailsCtrl', function($scope, ShoppingCart,$ionicNavBarDelegate) {
  
   $scope.pageTitle = "Your Delivery Details";
   $scope.cart = ShoppingCart.cart;
   $ionicNavBarDelegate.showBackButton(true);

  
})

/*
 * $Controller:Order Complete
 */
.controller('OrderCompleteCtrl', function($scope, Order, ShoppingCart, $ionicPopup, $state) {
  


   $scope.pageTitle = "Order Complete";
   $scope.cart = ShoppingCart.cart;
  
   //Order Complete
  // $scope.cart API Function to Store the Order Details for Customer
  
  
   
   var order = Order.createOrder($scope.cart);
   order.getCast().then(function (asyncCastData) {
           $scope.order = asyncCastData;
           $orderObj = $scope.order;
            $scope.order_id =   $orderObj.data.body.id;
           if ($orderObj.data.status.code == 1) {
            $state.go('tab.checkout-order-thanks',{ 'order_id':$orderObj.data.body.id});
           }else{
             $scope.msg = "There is some technical issue, Your order is not completed , please contact us";
           }
           
          //
           
    });

})




/*
 * $Controller:Order Thanks
 */
.controller('OrderThanksCtrl', function($scope,ShoppingCart,$state, $stateParams, $ionicNavBarDelegate, $route,$ionicHistory ) {
  
    $scope.changeState = function ($url,$id) {
      $state.go($url,{'order_id':$id});
    };
  
   $scope.pageTitle = "Order Thanks";
   $scope.cart = ShoppingCart.cart;
     
   $scope.order_id =   $stateParams.order_id;
   $ionicNavBarDelegate.showBackButton(false);
    
   $ionicHistory.clearHistory();
   $ionicHistory.clearCache();
  //$route.reload();
   //Order Complete

})

/*
 * $Controller:Order Complete
 */
.controller('OrderTrackFormCtrl', function($scope, Order, $stateParams, $ionicNavBarDelegate) {
   $scope.order_id =   $stateParams.order_id;
   $scope.pageTitle = "Order Track";
   //console.log($stateParams);
   //console.log($stateParams.order_id);
  /* var order = Order.getOrder($scope.order_id);
   order.getCast().then(function (asyncCastData) {
           
           $scope.order = asyncCastData;
           
    });*/

})

/*
 * $Controller:Order Complete
 */
.controller('OrderTrackCtrl', function($scope, Order, $stateParams, $ionicNavBarDelegate) {
   $scope.order_id =   $stateParams.order_id;
   $scope.pageTitle = "Order Track";
   //console.log($stateParams);
   //console.log($stateParams.order_id);
  /* var order = Order.getOrder($scope.order_id);
   order.getCast().then(function (asyncCastData) {
           
           $scope.order = asyncCastData;
           
    });*/

})



.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope,$state) {
  
  $scope.changeState = function ($url) {
      $state.go($url);
    };
  
  
  $scope.settings = {
    enableFriends: true
  };
});
