angular.module('starter.services', [])



/**
 * A simple example service that returns some data.
 */
.factory('Ditems', function($resource,$http) {

    var Ditems = {
            getCast: function () {
                // $http returns a 'promise'
                 return $http.get("http://dta.dazzlingwebplanet.api/web/getMenus.php?token=977332315a1b2e16952f93d8ace3cd0a").then(function (response) {
                      return response.data;
                    });
            }
        };
   return Ditems;
  
})



/*
 *Service for Geting Menu Items by Menu Categories
 */

.factory('MenuItems', function($http) {
  
  return {
    all: function() {
      return "";
    },
    getMenuItemsByCategoryId: function(cId) {
        
        var MenuItemsList = {
            getCast: function () {
                // $http returns a 'promise'
                return $http.get("http://dta.dazzlingwebplanet.api/web/getDishes.php?latitude=50.969642&longitude=-1.363979&page=1&submenuid="+cId+"&token=977332315a1b2e16952f93d8ace3cd0a").then(function (response) {
                
                    return response.data;
                });
            }
        };
        
      return MenuItemsList;
    }
    
  }
  
  
})



/*
 *Service for Geting Menu Items by Menu Categories
 */

.factory('Order', function($http) {
  
  return {
    all: function() {
      return "";
    },
    createOrder: function(cartobj) {
        
        var Order = {
            getCast: function () {
                 
                      var email = 'gillguy81@gmail.com';
                      var password =  '123456';
                      var UrlLogin =  "http://dta.dazzlingwebplanet.api/web/login.php";
                      var UrlOrder = "http://dta.dazzlingwebplanet.api/web/createOrder.php";
                      var token = "977332315a1b2e16952f93d8ace3cd0a";
                      
                   
                    /* return  $http({
                                url: UrlLogin,
                                method: "POST",
                                data: {'email':email, 'password':password, 'token':token},
                            }).success(function (data, status, headers, config) {
                             */    
                                  //var cookie_session = data.session_name +' = '+ data.sessid;
                                  //var session_name = data.session_name;
                                  //console.log('--session_name',session_name);
                                  //console.log('User ='+data);
                                  
                                       
                                        return  $http({
                                          url: UrlOrder,
                                          method: "POST",
                                          data: {'cartobj':cartobj, 'token':token , 'userid':'0'},
                                          headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
                                          }).success(function (data, status, headers, config) {
                                              
                                             
                                              
                                              
                                            }).error(function (data, status, headers, config) {
                                                
                                                var result = status + ' ' + headers;
                                                return  result;
                         
                                          });
                                      
                                      
                                      
                            
                                 
                                 
                               
                                
                                
                                
                             /* }).error(function (data, status, headers, config) {
                                  
                                  console.log('--sessid'+status + ' ' + headers);
                                  var result = status + ' ' + headers;
                                  return  status + ' ' + headers;
           
                              });*/
              
                 
                     
                       
                                // return $http.post("http://dresto.local/api/order/rorder/create", {data:'data'}).then(function (response) {
                           
                                //    return response.data;
                         
                                //  });
                                  
                                  
                                  
                       
                      
                      
                      
                   
                      
                      
                      
                      
            }
        };
        
      return Order;
    }
    
  }
  
  
})



/**
 * create a data service that provides a store and a shopping cart that
 * will be shared by all views (instead of creating fresh ones for each view).
 */
.factory('ShoppingCart', function() {
  
  // create store
   // var myStore = new store();

    // create shopping cart
    var myCart = new shoppingCart("AngularStore");

    // enable PayPal checkout
    // note: the second parameter identifies the merchant; in order to use the 
    // shopping cart with PayPal, you have to create a merchant account with 
    // PayPal. You can do that here:
    // https://www.paypal.com/webapps/mpp/merchant
    myCart.addCheckoutParameters("PayPal", "paypaluser@youremail.com");

    // enable Google Wallet checkout
    // note: the second parameter identifies the merchant; in order to use the 
    // shopping cart with Google Wallet, you have to create a merchant account with 
    // Google. You can do that here:
    // https://developers.google.com/commerce/wallet/digital/training/getting-started/merchant-setup
    myCart.addCheckoutParameters("Google", "xxxxxxx",
        {
            ship_method_name_1: "UPS Next Day Air",
            ship_method_price_1: "20.00",
            ship_method_currency_1: "USD",
            ship_method_name_2: "UPS Ground",
            ship_method_price_2: "15.00",
            ship_method_currency_2: "USD"
        }
    );

    // enable Stripe checkout
    // note: the second parameter identifies your publishable key; in order to use the 
    // shopping cart with Stripe, you have to create a merchant account with 
    // Stripe. You can do that here:
    // https://manage.stripe.com/register
    myCart.addCheckoutParameters("Stripe", "pk_test_4XYxawQb5PqqUQdllzjw9aF1",
        {
            chargeurl: "https://localhost:1234/processStripe.aspx"
        }
    );

    // return data object with store and cart
    return {
        cart: myCart
    };
    
})





.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
