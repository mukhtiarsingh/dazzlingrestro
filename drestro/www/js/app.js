// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ionicShop','ngResource', 'ngRoute'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('login', {
      url: '/login',
      templateUrl: 'templates/tab-login.html',
      controller: 'LoginCtrl'
      
    })

  
  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'MenuCategoriesCtrl'
      }
    }
  })

  .state('tab.menu-items', {
      url: '/menu-items/:cId',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-menu-items.html',
          controller: 'MenuItemsCtrl'
        }
      }
    })
  
   .state('tab.checkout', {
    cache: false,
      url: '/checkout',
      views: {
        'tab-checkout': {
          templateUrl: 'templates/tab-checkout.html',
          controller: 'CheckoutCtrl'
        }
      }
    })
    
    .state('tab.checkout-order', {
      url: '/checkout/order',
      //cache: false,
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-order-details.html',
      controller: 'OrderDetailsCtrl'
      }
      }
    })
    
    .state('tab.checkout-order-your-details', {
      url: '/checkout/order/your-details',
      cache: true,
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-your-details.html',
      controller: 'YourDetailsCtrl'
      }
      }
    })
    
     .state('tab.checkout-order-your-delivery-details', {
      //cache: false,
      url: '/checkout/order/your-delivery-details',
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-your-delivery-details.html',
      controller: 'YourDeliveryDetailsCtrl'
      }
      }
    })
     
    .state('tab.checkout-order-complete', {
      cache: false,
      url: '/checkout/order/complete',
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-order-complete.html',
      controller: 'OrderCompleteCtrl'
      }
      }
    })
  
  .state('tab.checkout-order-thanks', {
      cache: false,
      url: '/checkout/order/thanks',
      params: {
          'order_id': '0'
        },
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-order-thanks.html',
      controller: 'OrderThanksCtrl'
      }
      }
    })
  
  
  .state('tab.account-order-track-form', {
      url: '/account/order/track-form',
      views: {
      'tab-account': {
      templateUrl: 'templates/tab-track-order-form.html',
      controller: 'OrderTrackFormCtrl'
      }
      }
    })
  
  .state('tab.track-order', {
      url: '/track/order',
      params: {
          'order_id': '0'
        },
      views: {
      'tab-checkout': {
      templateUrl: 'templates/tab-track-order.html',
      controller: 'OrderTrackCtrl'
      }
      }
    })
  
  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-track-order.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
 // $urlRouterProvider.otherwise('/tab/dash');
  $urlRouterProvider.otherwise('/login');

  
  
  
})



.directive( 'goClick', function ( $location ) {
  return function ( scope, element, attrs ) {
    var path;

    attrs.$observe( 'goClick', function (val) {
      path = val;
    });

    element.bind( 'click', function () {
      scope.$apply( function () {
        $location.path( path );
      });
    });
  };
})


